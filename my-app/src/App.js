
import Login from './components/login/Login'
import Signup from './components/signup/signup'
import React, { Component } from "react";
import { BrowserRouter, Route, Switch, Redirect } from "react-router-dom";
import "./App.css";
import Dashboard from "./components/dashboard/Dashboard";
import { v4 } from "uuid";
import axios from "axios";
import link from "./congif";
import "bootstrap/dist/css/bootstrap.min.css";

class App extends Component {
  state = {
    items: [],
    didLoad: false,
    shouldRedirect: false,
    cartCount: 0,
  };

  handelAddToCart = (id) => {
    //todo:take the id to which item is matched
    let currentItem = this.state.items.filter((eachItem) => id === eachItem.id);
    let incrementCartCount = this.state.cartCount + 1;
    this.setState({
      cartCount: incrementCartCount,
    });
    console.log(
      `you have added ${currentItem[0].item_name} with price ${currentItem[0].price}rs`
    );
  };

  redirectHandle = () => {
    if (this.state.shouldRedirect) {
      return <Redirect to="/" />;
    }
  };
  componentDidMount() {
    axios(`${link.ITEM_LINK}/items.json`)
      .then((items) => {
        let newItems = items.data.map((item) => {
          return {
            id: `${v4()}`,
            item_name: item.name,
            image: item.image,
            price: item.cost,
          };
        });
        this.setState({
          items: newItems,
          didLoad: true,
        });
      })
      .catch((error) => {
        //redirect to home page
        this.setState({
          shouldRedirect: true,
        });
        console.log(error);
      });
  }

  render() {
    return (
      <BrowserRouter>
        <Switch>
          <Route exact path='/'>
            <Signup />
          </Route>
          <Route path='/login'>
            <Login />
          </Route>
          <Route path="/dashboard">
            {this.redirectHandle()}
            <Dashboard
              items={this.state.items}
              handelAddToCart={this.handelAddToCart}
              didLoad={this.state.didLoad}
              cartCount={this.state.cartCount}
            />
          </Route>
        </Switch>
      </BrowserRouter>
    );
  }
}

export default App;
