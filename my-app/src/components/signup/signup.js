import React,{ Component } from 'react'
import Username from '../BasicComponents/Username'
import Password from '../BasicComponents/Password'
import Email from '../BasicComponents/Email'
import { Redirect } from 'react-router-dom'
import axios from 'axios'
import link from "../../congif";
import amazon from "../../images/amazonblac.png";
import { Image } from "react-bootstrap";

class Login extends Component{
  state = {
    Username: '',
    Email:'',
    Password: '',
    usererror:'',
    emailerror:'',
    passerror:'',
    submiterror:'',
    shouldRedirect: false
  }

  redirectHandle =()=>{
    if(this.state.shouldRedirect){
      return <Redirect to='/login'/>
    }
  }

  changeFunctionUsername = (element) => {
    this.setState({
      Username:element.target.value
    })
  }

  changeFunctionEmail = (element) => {
    this.setState({
      Email:element.target.value
    })
  }

  changeFunctionPassword = (element) => {
    this.setState({
      Password:element.target.value
    })
  }

  validationCheck = ()=> {
      let emailcomponent = this.state.Email.split('@')
    if(this.state.Username.length > 7 && this.state.Password.length > 7 && emailcomponent.length === 2){
      this.setState({
        usererror:'',
        passerror:'',
        emailerror:''
      })
      axios.get(`${link.USERS_LINK}/user.json`)
        .then((res)=>{
            let check = res.data.filter((element)=>{
              if(element.username===this.state.Username){
                return true
              }else{
                this.setState({
                  submiterror:'',
                })
                return false
              }
            })
            if(check.length > 0){
              this.setState({
                submiterror:'Username already exist',
                shouldRedirect: false,
              })
            }else{
              this.setState({
                submiterror:'',
                shouldRedirect: true,
              })
            }
            
            return check.length===0
        })
        .catch((err)=>{
            console.log(err)

        })
    }else {
      return false
    }
  }

  validation = ()=>{
    let check = this.validationCheck()
    if(check){
        
    }else{
      if(this.state.Username.length < 8){
        this.setState({
          usererror:'Invalid Username enter minimum 8 characters'
        })
      }else{
        this.setState({
          usererror:''
        })
      }
      if(this.state.Password.length < 8){
        this.setState({
          passerror:'Invalid Password enter minimum 8 characters'
        })
      }else{
        this.setState({
          passerror:''
        })
      }
      let emailcomponent = this.state.Email.split('@')
      if(emailcomponent.length!==2){
        console.log(emailcomponent)
        this.setState({
            emailerror:'Invalid Email'
          })
      }else{
        this.setState({
          emailerror:''
        })
      }
    }
  }


  render(){
    return (
      <div className="vcontainer">
        <Image src={amazon} height="100" />
        <div className='center1'>
          <p>Username</p>
          <Username user={this.state.Username} changeFunction={this.changeFunctionUsername}/>
          <span>{this.state.usererror}</span>
          <p>Email</p>
          <Email email={this.state.Email} changeFunction={this.changeFunctionEmail}/>
          <span>{this.state.emailerror}</span>
          <p>Password</p>
          <Password Pass={this.state.Password} changeFunction={this.changeFunctionPassword}/>
          <span>{this.state.passerror}</span>
          <button className='addToButton' onClick={this.validation}>Signup</button>
          <span>{this.state.submiterror}</span>
          <p> Have a account <a href='/login'>Login</a></p>
          {this.redirectHandle()}
        </div>
        </div>
    )
  }
}
export default Login