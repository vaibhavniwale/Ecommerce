import React from 'react'

export default function Email(props) {
  return (
    <input type='Email' placeholder='Email' value={props.email} onChange={props.changeFunction}/>
  )
}
