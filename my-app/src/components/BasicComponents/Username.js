import React from 'react'

export default function Username(props) {
  return (
    <input type='text' placeholder='username' value={props.user} onChange={props.changeFunction}/>
  )
}
