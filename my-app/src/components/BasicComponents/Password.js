import React from 'react'

export default function Password(props) {
  return (
    <input type='password' placeholder='password' value={props.pass} onChange={props.changeFunction}/>
  )
}
