import React, { Component } from "react";
import ItemCard from "./ItemCard";
import icon from "../../images/loader.gif";
import {
  Nav,
  Navbar,
  Image,
  Button,
  FormControl,
  Form,
} from "react-bootstrap";
import amazon from "../../images/amazon.png";

class Dashboard extends Component {
  handleLoad = () => {
    if (!this.props.didLoad) {
      return (
        <React.Fragment>
          <div className="loader">
            <img src={icon} alt="" />
          </div>
        </React.Fragment>
      );
    }
  };
  render() {
    return (
      <React.Fragment>
        <Navbar bg="dark" variant="dark" sticky="top">
          <Navbar.Brand href="#home">
            <Image src={amazon} height="30" />
          </Navbar.Brand>
          <Nav className="mr-auto">
            <Nav.Link href="#home">Home</Nav.Link>
            <Nav.Link href="#features">Features</Nav.Link>
            <Nav.Link href="#pricing">Pricing</Nav.Link>
          </Nav>
          <Form inline>
            <FormControl type="text" placeholder="Search" className="mr-sm-2" />
            <Button variant="outline-info">Search</Button>
          </Form>
          <Nav>
            <Nav.Link href="#deets">More deals</Nav.Link>
            <Nav.Link eventKey={2} href="#memes">
              <i className="fas fa-shopping-cart cartIcon"> </i>
              {this.props.cartCount}
            </Nav.Link>
          </Nav>
        </Navbar>

        <div className="container">
          {this.handleLoad()}
          {this.props.items.map((item) => {
            return (
              <ItemCard
                key={item.id}
                item={item}
                handelAddToCart={this.props.handelAddToCart}
              />
            );
          })}
        </div>
      </React.Fragment>
    );
  }
}

export default Dashboard;
