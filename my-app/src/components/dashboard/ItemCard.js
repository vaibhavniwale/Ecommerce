import React, { Component } from "react";
import { Image } from "react-bootstrap";

class ItemCard extends Component {
  handelClick = () => {
    this.props.handelAddToCart(this.props.item.id);
  };
  render() {
    return (
      <div className="ItemCard">
        <Image src={this.props.item.image} height="160" />
        <div className="nameTag">{this.props.item.item_name}</div>
        <div className="priceTag">&#8377;{this.props.item.price}</div>
        <button onClick={this.handelClick} className="addToCartButton">
          Add to cart
        </button>
      </div>
    );
  }
}

export default ItemCard;
