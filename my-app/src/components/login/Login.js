import React,{ Component } from 'react'
import { Redirect } from 'react-router-dom'
import Username from '../BasicComponents/Username'
import Password from '../BasicComponents/Password'
import axios from 'axios'
import link from "../../congif";
import amazon from "../../images/amazonblac.png";
import { Image } from "react-bootstrap";


class Login extends Component{
  state = {
    Username: '',
    Password: '',
    usererror: '',
    passerror: '',
    submiterror:'',
    shouldRedirect: false
  }

  redirectHandle =()=>{
    if(this.state.shouldRedirect){
      return <Redirect to='/dashboard'/>
    }
  }

  changeFunctionUsername = (element) => {
    this.setState({
      Username:element.target.value
    })
  }

  changeFunctionPassword = (element) => {
    this.setState({
      Password:element.target.value
    })
  }

  validationCheck = ()=> {
    if(this.state.Username.length > 7 && this.state.Password.length > 7){
      this.setState({
        usererror:'',
        passerror:''
      })
      axios.get(`${link.USERS_LINK}/user.json`)
        .then((res)=>{
            let check = res.data.filter((element)=>{
              if(element.username===this.state.Username && element.password===this.state.Password){
                return true
              }else{
                return false
              }
            })
            if(check.length > 0){
              this.setState({
                shouldRedirect: true,
              })
            }else{
              this.setState({
                submiterror:'Invalid User'
              })
            }
            return check.length===0
        })
        .catch((err)=>{
            console.log(err)
        })
      return true
    }else {
      return false
    }
  }

  validation = ()=>{
    let check = this.validationCheck()
    console.log(check)
    if(check){
      

    }else{
      if(this.state.Username.length < 8){
        this.setState({
          usererror:'Invalid Username enter minimum 8 characters'
        })
      }else{
        this.setState({
          usererror:''
        })
      }
      if(this.state.Password.length < 8){
        this.setState({
          passerror:'Invalid Password enter minimum 8 characters'
        })
      }else{
        this.setState({
          passerror:''
        })
      }
    }
  }

  render(){
    return (
      <div className="vcontainer">
        <Image src={amazon} height="100" />
        <div className='center'>
          <p>Username</p>
          <Username user={this.state.Username} changeFunction={this.changeFunctionUsername}/>
          <span>{this.state.usererror}</span>
          <p>Password</p>
          <Password Pass={this.state.Password} changeFunction={this.changeFunctionPassword}/>
          <span>{this.state.passerror}</span>
          <button className='addToButton' onClick={this.validation}>login</button>
          <span>{this.state.submiterror}</span>
          <p>Don't have a account <a href='/'>Signup</a></p>
          {this.redirectHandle()}
        </div>
        </div>
    )
  }
}
export default Login